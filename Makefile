AS = nasm
TRIPLET = x86_64-unknown-linux-gnu
CFLAGS =
ARFLAGS =
NASMFLAGS = -f elf64

build/libcco.a: build/cco.o build/cco-$(TRIPLET).o
	$(AR) $(ARFLAGS) -rcs $@ $^

parity: bin/parity

build/cco.o: src/cco.c src/cco.h
	mkdir -p build
	$(CC) -o $@ -c $< $(CFLAGS)

build/cco-$(TRIPLET).o: src/cco-$(TRIPLET).s
	mkdir -p build
	$(AS) -o $@ $< $(NASMFLAGS)

bin/%: examples/%.c build/libcco.a
	mkdir -p bin
	$(CC) -o $@ $^ $(CFLAGS)

clean:
	rm -rf build/* bin/*
