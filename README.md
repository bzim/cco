# CCO - C Coroutines

C stackful Coroutines implemented with assembly help. Currently, only GNU/Linux x86-64 and i386 are supported. It may not work with optimizations due to undefined behavior.

# API

Consists of 13 items:
* `cco.h` header.
* `cco.a` library.
* `struct cco_stack` type. The type of heap allocated stacks.
* `struct cco_sp` type. The type of stack points.
* `struct cco_rot` type. The type of coroutines.
* `CCO_PAGE_SIZE` constant macro. The size of a page.
* `CCO_INIT_ROT(function_pointer, number_of_pages)` macro. Constant initialization for coroutines.
* `cco_alloc` function, which allocates a stack.
* `cco_free` function, which frees a stack.
* `cco_run` function, which runs an heap-allocated stack.
* `cco_resume` function, which resumes a stack point.
* `cco_call_rot` function, which calls/yields to a coroutine.
* `cco_free_rot` function, which (possibly) kill a coroutine and (certainly) free resources.
