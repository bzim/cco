#include "../src/cco.h"
#include <stdlib.h>
#include <stdio.h>

static void *is_even_start(void *arg);
static void *is_odd_start(void *arg);

static struct cco_rot is_even = CCO_INIT_ROT(is_even_start, 8);
static struct cco_rot is_odd = CCO_INIT_ROT(is_odd_start, 8);

int main(int argc, char **argv) {
  if (argc < 2) {
    fputs("Missing number argument\n", stderr);
    return -1;
  }
  long unsigned n = 0;
  sscanf(argv[1], "%lu", &n);
  if (cco_call_rot(&is_even, &n) != NULL) {
    puts("even");
  } else {
    puts("odd");
  }
  return 0;
}

static void *is_even_start(void *arg) {
  long unsigned *n = arg;
  while (*n > 0) {
    (*n)--;
    n = cco_call_rot(&is_odd, n);
  }
  return arg;
}

static void *is_odd_start(void *arg) {
  long unsigned *n = arg;
  while (*n > 0) {
    (*n)--;
    n = cco_call_rot(&is_even, n);
  }
  return NULL;
}
