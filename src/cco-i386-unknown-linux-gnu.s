bits 32

global cco_run
global cco_resume

extern exit
extern abort

section .text

cco_run:
    push ebp                    ; save registers
    mov ebp, esp
    push ebx
    push esi
    push edi
    push dword [ebp + 20]       ; save pointer to future caller
    mov eax, esp                ; save the current stack pointer
    mov esp, [ebp + 8]          ; set the first argument as stack
    push eax                    ; new second argument (old stack)
    push dword [ebp + 16]       ; third to first argument
    call dword [ebp + 12]       ; call the coroutine function
    add esp, 8                  ; pop the two passed arguments
    push eax                    ; passing the result as argument to exit
    call exit wrt ..plt         ; call exit
    jmp abort wrt ..plt         ; call abort if exit fails

cco_resume:
    push ebp                    ; save registers
    mov ebp, esp
    push ebx
    push esi
    push edi
    push dword [ebp + 16]       ; save pointer to future caller
    mov eax, [ebp + 12]         ; set second argument as return value
    mov edx, esp                ; save stack pointer
    mov esp, [ebp + 8]          ; set sp as first argument
    pop ecx
    pop edi
    pop esi
    pop ebx
    pop ebp
    mov [ecx], edx
    ret
