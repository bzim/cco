#include "cco.h"
#include <stdlib.h>
#include <stdbool.h>

static struct cco_sp *main_rot = NULL;
static struct cco_rot *curr_rot = NULL;

struct cco_stack *cco_alloc(size_t pages) {
  size_t len = pages * CCO_PAGE_SIZE;
  return (struct cco_stack *) ((char *) malloc(len) + len);
}

void cco_free(struct cco_stack *stack, size_t pages) {
  free((char *) stack - pages * CCO_PAGE_SIZE);
}

struct call {
  struct cco_rot *rot;
  void *arg;
};

static int corot(void *arg, struct cco_sp *caller) {
  struct call *call = arg;
  if (main_rot == NULL) {
    main_rot = caller;
  }
  if (curr_rot != NULL) {
    curr_rot->_stopped = caller;
  }
  curr_rot = call->rot;
  void *ret = call->rot->_fn(call->arg);
  cco_resume(main_rot, ret, &caller);
  return 0;
}

void *cco_call_rot(struct cco_rot *rot, void *arg) {
  struct cco_rot *self = curr_rot;
  void *ret;
  struct cco_sp *caller;
  struct call call = {.arg = arg, .rot = rot};
  if (rot->_alloc == NULL) {
    rot->_alloc = cco_alloc(rot->_pages);
    if (rot->_alloc == NULL) {
      abort();
    }
    ret = cco_run(rot->_alloc, corot, &call, &caller);
  } else {
    ret = cco_resume(rot->_stopped, arg, &caller);
  }
  curr_rot->_stopped = caller;
  curr_rot = self;
  return ret;
}

void cco_free_rot(struct cco_rot *rot) {
  if (rot->_alloc != NULL) {
    cco_free(rot->_alloc, rot->_pages);
    rot->_alloc = NULL;
  }
}
