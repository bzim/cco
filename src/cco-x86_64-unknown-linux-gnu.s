bits 64

global cco_run
global cco_resume

extern exit
extern abort

section .text

cco_run:
    push rbp                    ; save registers
    push rbx
    push rcx                    ; pointer to future caller
    push r12
    push r13
    push r14
    push r15
    mov rcx, rsi                ; save the function argument into rcx
    mov rax, rsp                ; save the current stack pointer
    mov rsp, rdi                ; set the first argument as stack
    mov rdi, rdx                ; third to first argument
    mov rsi, rax                ; saved sp as second argument
    sub rsp, 8                  ; stack alignment
    call rcx                    ; call the coroutine function
    mov rdi, rax
    call exit wrt ..plt         ; call exit with the returned value
    add rsp, 8                  ; stack alignment
    jmp abort wrt ..plt         ; call abort if exit fails

cco_resume:
    push rbp                    ; save registers
    push rbx
    push rdx                    ; pointer to future caller
    push r12
    push r13
    push r14
    push r15
    mov rdx, rsp                ; save stack pointer
    mov rsp, rdi                ; set sp as first argument
    pop r15
    pop r14
    pop r13
    pop r12
    pop rcx
    pop rbx
    pop rbp
    mov rax, rsi                ; return value of type void *
    mov [rcx], rdx              ; set "future caller" as previous rsp
    ret
