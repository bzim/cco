#ifndef CCO_H
#define CCO_H 1

#include <stddef.h>

#define CCO_PAGE_SIZE 0x1000

struct cco_stack;
struct cco_sp;

struct cco_stack *cco_alloc(size_t pages);

void cco_free(struct cco_stack *stack, size_t pages);

void *cco_run(struct cco_stack *stack,
              int (*callee)(void *arg, struct cco_sp *caller),
              void *arg,
              struct cco_sp /*out*/ **caller);

void *cco_resume(struct cco_sp *callee,
                 void *arg,
                 struct cco_sp /*out*/ **caller);

#define CCO_INIT_ROT(fn, pages) \
  {._fn = (fn), ._pages = (pages), ._alloc = NULL, ._stopped = NULL}

struct cco_rot {
  size_t _pages;
  struct cco_stack *_alloc;
  struct cco_sp *_stopped;
  void *(*_fn)(void *arg);
};

void *cco_call_rot(struct cco_rot *rot, void *arg);
void cco_free_rot(struct cco_rot *rot);

#endif
